<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    public function __construct()
    {
/*
        $this->middleware(['auth:api'])
            ->except([
                'login'
            ]);
*/
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required'],
        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }


        $user = User::where('email', $request->input('email'))->first();

        if ($user) {

            if (Hash::check($request->input('password'), $user->password)) {

                $token = $user->createToken('Personal Access Client')->accessToken;

                $user['access_token'] = $token;


                return Response::json(array('success' => ['user' => $user]), 200);
            }
        }

        return Response::json(array('error' => "Usuario/contraseña incorrecta"), 400);
    }

    public function logout()
    {
        $accessToken = Auth::user()->token();

        $accessToken->revoke();

        return Response::json(array('success' => "Sesión finalizada"), 200);
    }

    public function getAuth()
    {

        User::getImage(Auth::user());

        return Response::json(array('success' => Auth::user()), 200);
    }


}
