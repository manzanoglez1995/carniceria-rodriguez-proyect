<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'image' => 'required|image',
        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $unique = Category::where("name", $request->name)->where("active", true)->first();
        if ($unique) {
            return Response::json(array('error' => "Categoría existente"), 400);
        }


        $image = Storage::disk('public')->putFile('categories_img', $request->file('image'));

        $category = Category::create([
            'name' => $request->name,
            'image' => $image,
        ]);

        if ($category) {
            return Response::json(array('success' => ['user' => $category]), 200);
        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
        }

    }

    function read(Request $request)
    {

        if ($request->all) {
            $categories = Category::where("active", true)->orderBy('id', "DESC")->get();
            return Response::json(array('success' => $categories), 200);
        }

        $search = $request->search;

        $perPage = $request->per_page ? $request->per_page : 5;

        $categories = Category::search($search)
            ->where("active", true)
            ->orderBy('id', "DESC")
            ->paginate($perPage);

        return Response::json(array('success' => $categories), 200);
    }

    function update(Category $category, Request $request)
    {
        $validator = Validator::make($request->all(), array('name' => 'required'));
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $category->name = $request->name;

        if ($request->has('image')) {
            $validator = Validator::make($request->all(), array('image' => ['required', 'image']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }

            if (Storage::disk('public')->exists($category->image)) {
                Storage::disk('public')->delete($category->image);
            }
            $category->image = Storage::disk('public')->putFile('categories_img', $request->file('image'));
        }

        $category->save();

        return Response::json(array('success' => $category), 200);


    }

    function delete(Category $category)
    {
        $category->active = false;

        $category->save();

        return Response::json(array('success' => 'Deleted'), 200);
    }

    function topTen()
    {
        $categories = Category::where("active", true)
            ->orderBy('id', "DESC")
            ->get();

        $categories->each(function ($category) {
            $category["products"] = Product::where("category_id", $category->id)->limit(10)->get();
        });

        return Response::json(array('success' => $categories), 200);
    }
}
