<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Costumer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CostumerController extends Controller
{
    function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'telephone' => ['required', 'numeric'],
            'password' => ['required', 'min:6'],
            'confirm_password' => ['required', 'min:6', 'same:password'],

        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $image = null;
        if ($request->has('image') && $request->file('image')) {
            $image = Storage::disk('public')->putFile('costumers_img', $request->file('image'));
        }

        $costumer = Costumer::create([
            'user_id' => User::create([
                'name' => $request->name,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'password' => Hash::make($request->password),
                'role' => User::ROLES[0]
            ])->id,
            'image' => $image,
        ]);

        if ($costumer) {

            $costumer->user;

            return Response::json(array('success' => ['user' => $costumer]), 200);

        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
        }

    }

    function read(Request $request)
    {
        $search = $request->search;

        $perPage = $request->per_page ? $request->per_page : 5;

        $users = User::search($search)
            ->where('role', "costumer")
            ->with("costumer")
            ->with("orders")
            ->orderBy('id', "DESC")
            ->paginate($perPage);


        return Response::json(array('success' => $users), 200);
    }

    function update(Costumer $costumer, Request $request)
    {
        $user = $costumer->user;

        if ($request->has('name')) {

            $validator = Validator::make($request->all(), array('name' => 'required'));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->name = $request->name;
        }

        if ($request->has('email')) {

            $validator = Validator::make($request->all(), array('email' => ['required', 'string', 'email', 'max:255', "unique:users,email,$user->id"]));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->email = $request->email;
        }

        if ($request->has('role')) {

            $validator = Validator::make($request->all(), array('role' => ['required', 'numeric', 'max:3']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->role = $request->role;
        }

        if ($request->has('telephone')) {

            $validator = Validator::make($request->all(), array('telephone' => 'required|numeric'));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->telephone = $request->telephone;
        }

        if ($request->has('image') && $request->file('image')) {

            if (Storage::disk('public')->exists($costumer->image)) {
                Storage::disk('public')->delete($costumer->image);
            }

            $costumer->image = Storage::disk('public')->putFile('costumers_img', $request->file('image'));

            $costumer->save();
        }


        if ($request->has(['password', 'confirm_password'])) {

            $validator = Validator::make($request->all(), array(
                'password' => 'required|min:6',
                'confirm_password' => 'required|min:6|same:password'));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return Response::json(array('success' => $costumer), 200);


    }

    function delete(Costumer $costumer)
    {


        $costumer->user->orders->map(function ($order) {
            $order->delete();
        });

        $costumer->user->delete();

        $costumer->delete();


        //Ir eliminando registros segun sus relacioness
        return Response::json(array('success' => 'Deleted'), 200);

    }
}
