<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    function update(User $user, Request $request)
    {

        if ($request->has('name')) {

            $validator = Validator::make($request->all(), array('name' => 'required'));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->name = $request->name;
        }

        if ($request->has('email')) {

            $validator = Validator::make($request->all(), array('email' => ['required', 'string', 'email', 'max:255', "unique:users,email,$user->id"]));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->email = $request->email;
        }

        if ($request->has('role')) {

            $validator = Validator::make($request->all(), array('role' => ['required', 'numeric', 'max:3']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->role = $request->role;
        }

        if ($request->has('telephone')) {

            $validator = Validator::make($request->all(), array('telephone' => 'required|numeric'));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->telephone = $request->telephone;
        }



        if ($request->has(['password', 'confirm_password'])) {

            $validator = Validator::make($request->all(), array(
                'password' => 'required|min:6',
                'confirm_password' => 'required|min:6|same:password'));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return Response::json(array('success' => $user), 200);


    }

}
