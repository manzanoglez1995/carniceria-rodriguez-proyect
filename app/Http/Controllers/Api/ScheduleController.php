<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ScheduleController extends Controller
{

    function read(Request $request)
    {
        $perPage = $request->per_page ? $request->per_page : 10;
        $search = $request->search;

        $schedule = Schedule::search($search)->orderBy('id', "ASC")->paginate($perPage);


        return Response::json(array('success' => $schedule), 200);


    }

    function update(Schedule $schedule)
    {
        $schedule->active = !$schedule->active;
        $schedule->save();
        return Response::json(array('success' => $schedule), 200);

    }

    function getAvailableSchedule()
    {
        $hour = (integer)Carbon::now()->format('H') + 1; // una hora extra de ventaja;

        $schedule = Schedule::where('active', true)->where("id",">",$hour)->get();

        return Response::json(array('success' => $schedule), 200);
    }


}
