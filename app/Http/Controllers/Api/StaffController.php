<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Staff;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'telephone' => ['required', 'numeric'],
            'password' => ['required', 'min:6'],
            'confirm_password' => ['required', 'min:6', 'same:password'],
            'job' => ['required', 'numeric', 'max:2'],

        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $image = null;
        if ($request->has('image') && $request->file('image')) {
            $image = Storage::disk('public')->putFile('users_img', $request->file('image'));
        }

        $staff = Staff::create([
            'user_id' => User::create([
                'name' => $request->name,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'password' => Hash::make($request->password),
                'role' => User::ROLES[1]
            ])->id,
            'image' => $image,
            'job' => $request->job,
        ]);

        if ($staff) {

            $staff->user;

            return Response::json(array('success' => ['user' => $staff]), 200);

        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
        }

    }

    function read(Request $request)
    {
        $search = $request->search;

        $perPage = $request->per_page ? $request->per_page : 5;

        $users = User::search($search)
            ->where('role', "Staff")
            ->with("staff")
            ->orderBy('id', "DESC")
            ->paginate($perPage);


        return Response::json(array('success' => $users), 200);
    }

    function update(Staff $staff, Request $request)
    {

        $user = $staff->user;

        if ($request->has('name')) {

            $validator = Validator::make($request->all(), array('name' => 'required'));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->name = $request->name;
        }

        if ($request->has('email')) {

            $validator = Validator::make($request->all(), array('email' =>  ['required', 'string', 'email', 'max:255', "unique:users,email,$user->id"]));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $user->email = $request->email;
        }

        if ($request->has('role')) {

            $validator = Validator::make($request->all(), array('role' => ['required', 'numeric', 'max:3']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }

            $user->role = $request->role;
        }

        if ($request->has('job')) {

            $validator = Validator::make($request->all(), array('job' => ['required', 'numeric', 'max:2']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }

            $staff->job = $request->job;

            $staff->save();

        }

        if ($request->has('telephone')) {

            $validator = Validator::make($request->all(), array('telephone' => 'required|numeric'));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }

            $user->telephone = $request->telephone;
        }

        if ($request->has(['password', 'confirm_password'])) {

            $validator = Validator::make($request->all(), array(
                'password' => 'required|min:6',
                'confirm_password' => 'required|min:6|same:password'));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }


            $user->password = Hash::make($request->password);
        }

        if ($request->has('image') && $request->file('image')) {

            if (Storage::disk('public')->exists($staff->image)) {
                Storage::disk('public')->delete($staff->image);
            }

            $staff->image = Storage::disk('public')->putFile('users_img', $request->file('image'));

            $staff->save();
        }


        $user->save();

        return Response::json(array('success' => $staff), 200);


    }

    function delete(Staff $staff)
    {
        if (Storage::disk('public')->exists($staff->image))
            Storage::disk('public')->delete($staff->image);

        $staff->user->delete();
        $staff->delete();

        return Response::json(array('success' => 'Deleted'), 200);

    }


}
