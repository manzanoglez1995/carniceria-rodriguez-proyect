<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\OrderProduct;
use App\Models\Orders;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{
    function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'address' => ['required', 'json'],
            'schedule' => ['required', 'integer'],
            'user_id' => ['required', 'numeric', 'exists:users,id'],
            'shoppingCart' => ['required', 'json'],
            'money_exchange' => ['required'],
        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $address = json_decode($request->address);
        $shoppingCart = json_decode($request->shoppingCart);

        $order = Orders::create([
            'address' => $address->address,
            'lat' => $address->lat,
            'lng' => $address->lng,
            'note' => $request->note ? $request->note : "Sin detalle",
            'state' => Orders::STATUS[0],
            'total' => $shoppingCart->total,
            'subtotal' => $shoppingCart->subtotal,
            'money_exchange' => $request->money_exchange,
            'countProducts' => $shoppingCart->count,
            'schedule_id' => (integer)$request->schedule,
            'user_id' => (integer)$request->user_id,
        ]);

        foreach ($shoppingCart->products as $product) {
            OrderProduct::create([
                'orders_id' => $order->id,
                'product_id' => $product->id,
                'price' => $product->price,
                'discount_percentage' => $product->discount,
                'quantity' => $product->qty,
                'import' => $product->import,
                'discount_price' => $product->discount_price,
            ]);
        }

        $order->products;

        return Response::json(array('success' => $order), 200);

    }

    function read(Request $request)
    {
        $perPage = $request->per_page ? $request->per_page : 10;
        $search = $request->search;
        $schedule = $request->schedule;
        $user_id = $request->user_id;
        $all = $request->all;

        $orders = Orders::search($search);

        if ($schedule) {
            $orders = $orders->where('schedule_id', $schedule);
        }
        if ($user_id) {
            $orders = $orders->where('user_id', $user_id);
        }else{
            $orders = $orders->active();
        }

        $orders = $orders->orderBy('id', "ASC")->orderBy('schedule_id', "DESC");
        $orders = $orders->with('costumer')->with('time');

        if ($all) {
            $orders = $orders->get();
        } else {
            $orders = $orders->paginate($perPage);
        }

        //OBTENIENDO HORARIOS DISPONIBLES
        return Response::json(array('success' => array(
            "orders" => $orders,
            "schedule" => Schedule::getHoursInOrders(),
        )), 200);
    }

    function edit(Orders $order, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => ['required']
        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $order->status = $request->status;

        $order->save();

        return Response::json(array('success' => $order), 200);
    }

    function cancel(Orders $order, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'note' => ['required']
        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $order->status = Orders::STATUS[3];
        $order->canceled_note = $request->note;
        $order->save();

        return Response::json(array('success' => "Orden cancelada"), 200);
    }
}
