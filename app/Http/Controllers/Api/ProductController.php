<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:150'],
            'description' => ['required', 'string'],
            'price' => ['required', 'integer', 'gte:1'],
            'discount' => ['required', 'integer'],
            'measure' => ['required', 'string'],
            'picture_1' => 'required|image',
            'picture_2' => 'image',
            'picture_3' => 'image',
            'picture_4' => 'image',
            'category_id' => ['required', 'integer'],
            'availability' => ['required', 'integer'],
        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $picture_1 = null;
        $picture_2 = null;
        $picture_3 = null;
        $picture_4 = null;

        $picture_1 = Storage::disk('public')->putFile('products_img', $request->file('picture_1'));
        if ($request->file('picture_2')) {
            $picture_2 = Storage::disk('public')->putFile('products_img', $request->file('picture_2'));
        }
        if ($request->file('picture_3')) {
            $picture_3 = Storage::disk('public')->putFile('products_img', $request->file('picture_3'));
        }
        if ($request->file('picture_4')) {
            $picture_4 = Storage::disk('public')->putFile('products_img', $request->file('picture_4'));
        }

        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'discount' => $request->discount,
            'measure' => $request->measure,
            'picture_1' => $picture_1,
            'picture_2' => $picture_2,
            'picture_3' => $picture_3,
            'picture_4' => $picture_4,
            'category_id' => $request->category_id,
            'availability' => $request->availability,
        ]);

        if ($product) {
            return Response::json(array('success' => ['product' => $product]), 200);
        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
        }

    }

    function read(Request $request)
    {

        $perPage = $request->per_page ? $request->per_page : 10;
        $search = $request->search;
        $search_category = (array)$request->search_category;

        $products = Product::search($search);

        if (count($search_category) > 0) {
            $products = $products->whereIn('category_id', $search_category);
        }

        $products = $products->with('category');

        $products = $products->active()->orderBy('id', "DESC")->paginate($perPage);

        return Response::json(array('success' => $products), 200);
    }

    function update(Product $product, Request $request)
    {

        if ($request->has('name')) {
            $validator = Validator::make($request->all(), array('name' => ['required', 'string', 'max:150']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $product->name = $request->name;
        }

        if ($request->has('description')) {
            $validator = Validator::make($request->all(), array('description' => ['required', 'string']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $product->description = $request->description;
        }

        if ($request->has('price')) {
            $validator = Validator::make($request->all(), array('price' => ['required', 'integer', 'min:1']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $product->price = $request->price;
        }

        if ($request->has('discount')) {
            $validator = Validator::make($request->all(), array('discount' => ['required', 'numeric']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $product->discount = $request->discount;
        }
        if ($request->has('measure')) {
            $validator = Validator::make($request->all(), array('measure' => ['required', 'string']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $product->measure = $request->measure;
        }

        if ($request->has('category_id')) {
            $validator = Validator::make($request->all(), array('discount' => ['required', 'integer']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $product->category_id = $request->category_id;
        }

        if ($request->has('availability')) {
            $validator = Validator::make($request->all(), array('discount' => ['required', 'integer']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }
            $product->availability = $request->availability;
        }

        if ($request->has('picture_1')) {
            $validator = Validator::make($request->all(), array('picture_1' => ['required', 'image']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }

            if (Storage::disk('public')->exists($product->picture_1)) {
                Storage::disk('public')->delete($product->picture_1);
            }

            $product->picture_1 = Storage::disk('public')->putFile('products_img', $request->file('picture_1'));
        }

        if ($request->has('picture_2')) {
            $validator = Validator::make($request->all(), array('picture_2' => ['required', 'image']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }

            if (Storage::disk('public')->exists($product->picture_2)) {
                Storage::disk('public')->delete($product->picture_2);
            }

            $product->picture_2 = Storage::disk('public')->putFile('products_img', $request->file('picture_2'));
        }

        if ($request->has('picture_3')) {
            $validator = Validator::make($request->all(), array('picture_3' => ['required', 'image']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }

            if (Storage::disk('public')->exists($product->picture_3)) {
                Storage::disk('public')->delete($product->picture_3);
            }

            $product->picture_3 = Storage::disk('public')->putFile('products_img', $request->file('picture_3'));
        }

        if ($request->has('picture_4')) {
            $validator = Validator::make($request->all(), array('picture_4' => ['required', 'image']));
            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }

            if (Storage::disk('public')->exists($product->picture_4)) {
                Storage::disk('public')->delete($product->picture_4);
            }

            $product->picture_4 = Storage::disk('public')->putFile('products_img', $request->file('picture_4'));
        }

        $product->save();

        return Response::json(array('success' => $product), 200);

    }

    function delete(Product $product)
    {
        $product->active = 0;
        $product->save();

        return Response::json(array('success' => 'Deleted'), 200);

    }

    function related_products(Product $product)
    {
        $products = Product::where("category_id", $product->category_id)
            ->where("id", "!=", $product->id)
            ->active()
            ->with("category")
            ->take(4)
            ->get();

        return Response::json(array('success' => $products), 200);

    }

    function topThreeReviews()
    {
        $products = Product::active()->with("category")->limit(3)->get();

        $products = $products->sortByDesc(function ($product) {
            return $product->reviews->avg("score");
        });

        return Response::json(array('success' => $products), 200);

    }

    function getShoppingCart(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ShopCart' => ['required', 'json']
        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $products = [];
        $total = 0;

        $products_json = json_decode($request->ShopCart);

        if (count($products_json) > 0) {
            foreach ($products_json as $item) {
                $product = Product::where("id", $item->item_id)->active()->first();

                if ($product) {
                    $product['qty'] = $item->quantity;

                    $product['original_import'] = round($product->price * $item->quantity);
                    $product['import'] = round($product->discount_price * $item->quantity);
                    $total += $product['import'];

                    $products[] = $product;
                }
            }
        }

        $subtotal = round(($total - ($total * 0.16)));

        return Response::json(array('success' => array(
            "items" => $products,
            "total" => $total,
            "subtotal" => $subtotal,
            "count" => count($products),
        )), 200);
    }

}
