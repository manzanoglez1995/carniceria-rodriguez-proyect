<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Reviews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ReviewsController extends Controller
{
    function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'review' => ['required', 'string'],
            'score' => ['required', 'numeric', 'min:0', 'max:5'],
            'product_id' => ['required', 'integer', 'exists:products,id'],

        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $review = Reviews::create($request->all());

        $review->product;

        return Response::json(array('success' => $review), 200);

    }

    function read(Request $request)
    {
        $perPage = $request->per_page ? $request->per_page : 10;
        $product_id = $request->product_id ;
        $search = $request->search;

        $reviews = Reviews::search($search)->where("product_id",$product_id)->orderBy('id', "DESC")->paginate($perPage);


        return Response::json(array('success' => $reviews), 200);
    }

    function delete(Reviews $review)
    {
        $review->delete();

        return Response::json(array('success' => $review), 200);
    }
}
