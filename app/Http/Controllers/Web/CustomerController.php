<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
//Users
    function customer()
    {
        return view("administrator.customers.show")->withPage('customers.show');
    }

    function  create()
    {
        return view("administrator.customers.form")->withPage('customers.create');
    }

    function  edit(User $user)
    {
        return view("administrator.customers.form")
            ->withUser($user)
            ->withPage('customers.edit')
            ->withParameter($user->id);
    }
}
