<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductsController extends Controller
{
    //Products
    function products()
    {
        return view("administrator.products.show")->withPage('products.show');
    }

    function create()
    {
        return view("administrator.products.form")
            ->withPage('products.create');
    }

    function edit(Product $product)
    {
        return view("administrator.products.form")
            ->withProduct($product)
            ->withPage('products.edit')
            ->withParameter($product->id);
    }


    function reviews($product_name)
    {
        $product = Product::where("name", $product_name)->first();



        if ($product) {
            return view("administrator.products.reviews.show")
                ->withProduct($product)
                ->withPage('products.reviews')
                ->withParameter($product->id);
        }

        return abort(404);
    }

}
