<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
//Categories
    function categories()
    {
        return view("administrator.categories.show")->withPage('categories.show');
    }

}
