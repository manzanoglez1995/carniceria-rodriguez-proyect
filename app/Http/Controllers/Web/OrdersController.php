<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Orders;

class OrdersController extends Controller
{
    //Orders
    function orders()
    {
        return view("administrator.orders.show")->withPage('orders.show');
    }

    function detail(Orders $order)
    {
        $order->costumer;

        $order->products->map(function ($product) {
            $product->info;
        });

        return view("administrator.orders.form")
            ->withParameter($order->id)
            ->withOrder($order)
            ->withPage('orders.detail');
    }

    function report(){
        return view("administrator.orders.report")->withPage('orders.report');
    }
}
