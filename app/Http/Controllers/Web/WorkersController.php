<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;

class WorkersController extends Controller
{
    //Users
    function workers()
    {
        return view("administrator.workers.show")->withPage('workers.show');
    }

    function create()
    {
        return view("administrator.workers.form")->withPage('workers.create');
    }

    function edit(User $user)
    {
        $user->staff;

        return view("administrator.workers.form")
            ->withUser($user)
            ->withPage('workers.edit')
            ->withParameter($user->id);
    }
}
