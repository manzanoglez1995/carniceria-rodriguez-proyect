<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Orders;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class AppController extends Controller
{

    //Dashboard
    function dashboard()
    {
        return view("administrator.dashboard")->withPage('dashboard');
    }

//Graficas
    function graphics()
    {

        $graphic1 = Category::where("active", true)->get();
        //
        $orders = Orders::all();

        $weekMap = [
            0 => 'Domingo',
            1 => 'Lunes',
            2 => 'Martes',
            3 => 'Miercoles',
            4 => 'Jueves',
            5 => 'Viernes',
            6 => 'Sabado',
        ];
        $graphic2 = [
            'Domingo' => 0,
            'Lunes' => 0,
            'Martes' => 0,
            'Miercoles' => 0,
            'Jueves' => 0,
            'Viernes' => 0,
            'Sabado' => 0,
        ];

        foreach ($orders as $order) {

            $created = new Carbon($order->created_at);

            $graphic2[$weekMap[$created->dayOfWeek]]++;
        }


        //
        $graphic3 = Orders::select(DB::raw('MonthName(created_at) as month, count(*) as count'))
            ->where(DB::raw('YEAR(created_at)'), Carbon::now()->year)
            ->groupBy(DB::raw('MonthName(created_at)'))
            ->get();

        return Response::json(array('success' => array(
            'graphic_1' => $graphic1,
            'graphic_2' => $graphic2,
            'graphic_3' => $graphic3,
        )), 200);
    }
}
