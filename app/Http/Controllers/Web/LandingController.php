<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\Product;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    //landing
    function landing()
    {
        return view("landing_page.index");
    }

    function products()
    {
        return view("landing_page.products");
    }

    function product(Product $product)
    {
        $product->category;

        return view("landing_page.product", compact('product'));
    }


    function shopping_cart()
    {
        return view("landing_page.shopping_cart");
    }

    function orders()
    {
        return view("landing_page.orders");
    }

    function order_detail(Orders $order)
    {
        $order->costumer;

        $order->products->map(function ($product) {
            $product->info;
        });

         return view("landing_page.order",compact('order'));
    }


    function profile()
    {
        return view("landing_page.profile");
    }

}
