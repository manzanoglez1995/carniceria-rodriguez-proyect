<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Costumer extends Model
{
    protected $fillable = ['user_id','image'];

    protected $hidden = ['image','address_id','preferred_address_id','shopping_cart_id'];

    protected $appends = ['image_path', 'address', 'preferred_address', 'shopping_cart'];

    //Relations
    function user()
    {
        return $this->belongsTo(User::class);
    }


    //Attributes
    function getImagePathAttribute()
    {
        if (Storage::disk('public')->exists($this->image)) {

            $image = Storage::url($this->image);
        } else {
            $image = asset('admin/img/default-user.svg');
        }

        return $image;
    }

    function getAddressAttribute()
    {
        if ($this->address_id) {
            return "";
        }

        return 0;
    }

    function getPreferredAddressAttribute()
    {
        if ($this->preferred_address_id) {
            return "";
        }

        return 0;
    }

    function getShoppingCartAttribute()
    {
        if ($this->shopping_cart_id) {
            return "";
        }

        return 0;
    }


}
