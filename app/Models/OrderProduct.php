<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $fillable = [
        'orders_id',
        'product_id',
        'price',
        'discount_percentage',
        'quantity',
        'discount_price',
        'import'
    ];

    public function info()
    {
        return $this->belongsTo(Product::class,'product_id');
    }


}
