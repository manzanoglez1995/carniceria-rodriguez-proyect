<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    public const STATUS = ["Enviado", "En camino", "Recibido", "Cancelado", "Conflicto"];

    protected $fillable = [
        'address',
        'lat',
        'lng',
        'note',
        'status',
        'total',
        'subtotal',
        'money_exchange',
        'countProducts',
        'schedule_id',
        'user_id',
    ];

    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'orders_id');
    }

    public function costumer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function time()
    {
        return $this->belongsTo(Schedule::class, 'schedule_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('id', 'like', "%$search%")
            ->Orwhere('address', 'like', "%$search%")
            ->Orwhere('status', 'like', "%$search%")
            ->Orwhere('note', 'like', "%$search%")
            ->Orwhere('total', 'like', "%$search%")
            ->OrWhereHas('costumer', function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });

    }

    public function scopeActive($query)
    {
        return $query->whereIn('status',[self::STATUS[0],self::STATUS[1],self::STATUS[4]]);
    }

}
