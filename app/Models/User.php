<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable;

    public const ROLES = ['Costumer', 'Staff', 'Administrator'];

    protected $fillable = [
        'name', 'email', 'telephone', 'password', 'image', 'role',
    ];
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at', 'image'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Relations
    function costumer()
    {
        return $this->hasOne(Costumer::class);
    }

    function staff()
    {
        return $this->hasOne(Staff::class);
    }

    function orders(){
        return $this->hasMany(Orders::class,'user_id');
    }


    //scopes
    public function scopeSearch($query, $search)
    {
        return $query
            ->where('name', 'like', "%$search%")
            ->Orwhere('email', 'like', "%$search%")
            ->Orwhere('telephone', 'like', "%$search%")
            ->Orwhere('role', 'like', "%$search%")
            ->OrwhereHas('staff', function ($query) use ($search) {
                $query->where('job', 'like', "%$search%");
            });
    }


    static function getImage($user)
    {
        if (Storage::disk('public')->exists($user->image)) {

            $user->image = Storage::url($user->image);
        } else {
            $user->image = asset('admin/img/default-user.svg');
        }

        return $user->image;
    }

}
