<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Category extends Model
{
    protected $fillable = [
        'name','image'
    ];

    protected $hidden = ['active','image'];

    protected $appends = ['image_path','products_count'];


    function products()
    {
        return $this->hasMany(Product::class);
    }

//Attributes

    public function getImagePathAttribute()
    {
        if (Storage::disk('public')->exists($this->image)) {

            $image = Storage::url($this->image);
        } else {
            $image = asset('admin/img/prev-product.png');
        }
        return $image;
    }

    public function getProductsCountAttribute()
    {

        return  $this->products->where("active",true)->count();
    }

    //scopes
    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'like', "%$search%");
    }

}
