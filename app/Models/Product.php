<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    protected $fillable = [
        'name', 'description', 'price', 'discount','measure', 'category_id', 'availability', 'picture_1', 'picture_2', 'picture_3', 'picture_4'
    ];

    protected $hidden = ['active', 'picture_1', 'picture_2', 'picture_3', 'picture_4'];

    protected $appends = ['discount_price', 'images_path', 'overall_score'];

    //relations

    function category()
    {
        return $this->belongsTo(Category::class);
    }

    function reviews()
    {
        return $this->hasMany(Reviews::class);
    }


    //Attributes

    public function getDiscountPriceAttribute()
    {
        $price = $this->price;
        $discount = $this->discount;

        if ($discount > 0) {
            $price = round($price - ($price * (($discount / 100))));
        }

        return $price;
    }

    public function getOverallScoreAttribute()
    {
        $score = $this->reviews->avg("score");
        return $score;
    }

    public function getImagesPathAttribute()
    {
        $files = array();

        $files["picture_1"] = Storage::disk('public')->exists($this->picture_1) ?
            Storage::url($this->picture_1) : asset('admin/img/prev-product.png');

        $files["picture_2"] = Storage::disk('public')->exists($this->picture_2) ?
            Storage::url($this->picture_2) : null;
        $files["picture_3"] = Storage::disk('public')->exists($this->picture_3) ?
            Storage::url($this->picture_3) : null;

        $files["picture_4"] = Storage::disk('public')->exists($this->picture_4) ?
            Storage::url($this->picture_4) : null;


        return (object)$files;
    }

    //scopes
    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'like', "%$search%")
            ->OrWhereHas('category', function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
    }


    public function scopePrice($query, $min, $max)
    {
        if ($min == 0 && $max == 0) {
            return $query->whereBetween('price', array(0, 99999999));
        }

        return $query->whereBetween('price', array($min, $max));
    }

    public function scopeActive($query)
    {
        return $query->where("active", true);
    }


    public function scopeCategory($query, $category_id)
    {
        return $query->where("category_id", $category_id);
    }

}
