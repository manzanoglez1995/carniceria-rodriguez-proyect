<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    protected $fillable = ['name', 'email', 'review', 'score', 'product_id'];

    function product()
    {
        return $this->belongsTo(Product::class);
    }


    //scopes
    public function scopeSearch($query, $search)
    {
        return $query
            ->where('name', 'like', "%$search%")
            ->where('email', 'like', "%$search%");

    }

}
