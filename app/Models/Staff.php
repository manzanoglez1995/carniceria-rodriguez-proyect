<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Staff extends Model
{

    public const JOBS = ['repartidor', 'administrador'];

    protected $fillable = ['user_id', 'job', 'image'];

    protected $hidden = ['image'];

    protected $appends = ['image_path'];

    //Relations
    function user()
    {
        return $this->belongsTo(User::class);
    }


    //Attributes

    public function getImagePathAttribute()
    {
        if (Storage::disk('public')->exists($this->image)) {

            $image = Storage::url($this->image);
        } else {
            $image = asset('admin/img/default-user.svg');
        }

        return $image;
    }

}
