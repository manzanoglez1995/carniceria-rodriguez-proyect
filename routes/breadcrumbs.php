<?php
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;


//Administrador backend
Breadcrumbs::for('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard'));
});


// dashboard > Ordenes
Breadcrumbs::for('orders.show', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Ordenes', route('orders.show'));
});
// dashboard > Ordenes > detalle
Breadcrumbs::for('orders.detail', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('orders.show');
    $breadcrumbs->push('Detalle', route('orders.detail',$id));
});

// dashboard > Ordenes > reporte
Breadcrumbs::for('orders.report', function ($breadcrumbs) {
    $breadcrumbs->parent('orders.show');
    $breadcrumbs->push('Reporte', route('orders.report'));
});



// dashboard > Categorías
Breadcrumbs::for('categories.show', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Categorías', route('categories.show'));
});


// dashboard > Categorías
Breadcrumbs::for('config.schedule', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Configuración', route('config.schedule'));
});


// dashboard > costumer
Breadcrumbs::for('customers.show', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Clientes', route('customers.show'));
});
// dashboard > costumer > nuevo
Breadcrumbs::for('customers.create', function ($breadcrumbs) {
    $breadcrumbs->parent('customers.show');
    $breadcrumbs->push('Crear', route('customers.create'));
});
// dashboard > costumer > editar[id]
Breadcrumbs::for('customers.edit', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('customers.show');
    $breadcrumbs->push('Editar', route('customers.edit',$id));
});


// dashboard > workers
Breadcrumbs::for('workers.show', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Empleados', route('workers.show'));
});
// dashboard > workers > nuevo
Breadcrumbs::for('workers.create', function ($breadcrumbs) {
    $breadcrumbs->parent('workers.show');
    $breadcrumbs->push('Crear', route('workers.create'));
});
// dashboard > workers > editar[id]
Breadcrumbs::for('workers.edit', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('workers.show');
    $breadcrumbs->push('Editar', route('workers.edit',$id));
});


// dashboard > Productos
Breadcrumbs::for('products.show', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Productos', route('products.show'));
});
// dashboard > Productos > nuevo
Breadcrumbs::for('products.create', function ($breadcrumbs) {
    $breadcrumbs->parent('products.show');
    $breadcrumbs->push('Crear', route('products.create'));
});
// dashboard > Productos > editar[id]
Breadcrumbs::for('products.edit', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('products.show');
    $breadcrumbs->push('Editar', route('products.edit',$id));
});
// dashboard > Productos > reviews[id]
Breadcrumbs::for('products.reviews', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('products.show');
    $breadcrumbs->push('Comentarios', route('products.reviews',$id));
});
