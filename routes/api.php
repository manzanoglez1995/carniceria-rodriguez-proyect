<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'users'], function () {

    $controller = 'Api\UserController@';
     Route::post('/update/{user}', $controller . 'update');


    Route::group(['prefix' => 'staff'], function () {

        $controller = 'Api\StaffController@';

        Route::post('/create', $controller . 'create');
        Route::get('/show', $controller . 'read');
        Route::post('/update/{staff}', $controller . 'update');
        Route::delete('/delete/{staff}', $controller . 'delete');

    });

    Route::group(['prefix' => 'costumer'], function () {

        $controller = 'Api\CostumerController@';

        Route::post('/create', $controller . 'create');
        Route::get('/show', $controller . 'read');
        Route::post('/update/{costumer}', $controller . 'update');
        Route::delete('/delete/{costumer}', $controller . 'delete');

    });


});

Route::group(['prefix' => 'categories'], function () {
    $controller = 'Api\CategoryController@';
    Route::post('/create', $controller . 'create');
    Route::get('/show', $controller . 'read');
    Route::post('/update/{category}', $controller . 'update');
    Route::delete('/delete/{category}', $controller . 'delete');
    Route::get('/topTen', $controller . 'topTen');
 });

Route::group(['prefix' => 'products'], function () {

    $controller = 'Api\ProductController@';

    Route::post('/create', $controller . 'create');
    Route::get('/show', $controller . 'read');
    Route::post('/update/{product}', $controller . 'update');
    Route::delete('/delete/{product}', $controller . 'delete');
    Route::get('/related_products/{product}', $controller . 'related_products');
    Route::get('/topThreeReviews', $controller . 'topThreeReviews');
    Route::post('/getShoppingCart', $controller . 'getShoppingCart');

});

Route::group(['prefix' => 'reviews'], function () {

    $controller = 'Api\ReviewsController@';

    Route::post('/create', $controller . 'create');
    Route::get('/show', $controller . 'read');
    Route::delete('/delete/{review}', $controller . 'delete');

});

Route::group(['prefix' => 'schedule'], function () {

    $controller = 'Api\ScheduleController@';

    Route::get('/show', $controller . 'read');
    Route::get('/getAvailableSchedule', $controller . 'getAvailableSchedule');
    Route::post('/update/{schedule}', $controller . 'update');


});

Route::group(['prefix' => 'order'], function () {

    $controller = 'Api\OrdersController@';

      Route::get('/show/{order?}', $controller . 'read');
     Route::post('/create', $controller . 'create');
     Route::post('/edit/{order}', $controller . 'edit');
     Route::delete('/cancel/{order}', $controller . 'cancel');

});


Route::get('/graphics', 'Web\AppController@graphics');


