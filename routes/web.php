<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', "Web\LandingController@landing")->name('landing');
Route::get('/productos', "Web\LandingController@products")->name('products');
Route::get('/carrito', "Web\LandingController@shopping_cart")->name('shopping_cart');
Route::get('/producto/{product}', "Web\LandingController@product")->name('product');



Route::group(['middleware' => ['auth']], function () {

    Route::get('/pedidos', "Web\LandingController@orders")->name('orders');
    Route::get('/pedido/{order}', "Web\LandingController@order_detail")->name('order_detail');
    Route::get('/perfil' , "Web\LandingController@profile")->name('profile');


    Route::group(['prefix' => 'dashboard'], function () {

        $controller = "Web\AppController";
        Route::get('/', "$controller@dashboard")->name('dashboard');

        Route::group(['prefix' => 'pedidos'], function () {
            $controller = "Web\OrdersController";
            Route::get('/', "$controller@orders")->name('orders.show');
            Route::get('/detalle/{order}', "$controller@detail")->name('orders.detail');
            Route::get('/reporte', "$controller@report")->name('orders.report');
        });


        Route::group(['middleware' => ['isAdmin']], function () {

            Route::group(['prefix' => 'productos'], function () {
                $controller = "Web\ProductsController";
                Route::get('/', "$controller@products")->name('products.show');
                Route::get('/nuevo', "$controller@create")->name('products.create');
                Route::get('/editar/{product}', "$controller@edit")->name('products.edit');
                Route::get('/comentarios/{product_name}', "$controller@reviews")->name('products.reviews');
            });

            Route::group(['prefix' => 'categorias'], function () {
                $controller = "Web\CategoriesController";
                Route::get('/', "$controller@categories")->name('categories.show');
            });

            Route::group(['prefix' => 'empleados'], function () {
                $controller = "Web\WorkersController";
                Route::get('/', "$controller@workers")->name('workers.show');
                Route::get('/nuevo', "$controller@create")->name('workers.create');
                Route::get('/editar/{user}', "$controller@edit")->name('workers.edit');
            });

            Route::group(['prefix' => 'clientes'], function () {
                $controller = "Web\CustomerController";
                Route::get('/', "$controller@customer")->name('customers.show');
                Route::get('/nuevo', "$controller@create")->name('customers.create');
                Route::get('/editar/{user}', "$controller@edit")->name('customers.edit');
            });

            Route::group(['prefix' => 'horarios'], function () {
                $controller = "Web\ConfigController";
                Route::get('/', "$controller@schedule")->name('config.schedule');
              });
        });

    });
});
