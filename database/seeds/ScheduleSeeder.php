<?php

use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\Schedule::HOURS as $hour) {
            factory(\App\Models\Schedule::class)->create([
                'hours' => $hour,
            ]);
        }

    }
}
