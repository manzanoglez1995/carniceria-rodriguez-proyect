<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('address');
            $table->string('lat');
            $table->string('lng');
            $table->text('note')->nullable();
            $table->text('canceled_note')->nullable();
            $table->decimal('total', 8, 2);
            $table->decimal('subtotal', 8, 2);
            $table->decimal('money_exchange', 8, 2)->default(0);
            $table->integer('countProducts');
            $table->unsignedBigInteger('schedule_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('shopping_cart_id')->nullable();
            $table->enum('status', \App\Models\Orders::STATUS)->default(\App\Models\Orders::STATUS[0]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
