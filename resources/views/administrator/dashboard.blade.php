@extends('administrator.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-tachometer"></i><label>Dashboard</label>
        </div>
        <div class="card-body">
            <div class="content">
                <dashboard/>
            </div>
        </div>
    </div>

@endsection

