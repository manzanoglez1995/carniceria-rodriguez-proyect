@extends('administrator.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-edit"></i><label>Productos</label>
        </div>
        <div class="card-body">
            <div class="content">
           <form-products data_edit="{{isset($product) ? $product : null }}"></form-products>
            </div>
        </div>
    </div>

@stop
