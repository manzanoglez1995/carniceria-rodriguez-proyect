@extends('administrator.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-clock"></i><label>Horarios</label>
        </div>
        <div class="card-body">
            <div class="content">
                <schedule/>
            </div>
        </div>
    </div>

@endsection

