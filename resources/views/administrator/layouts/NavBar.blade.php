<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{ route('landing') }}">


        <img class="navbar-brand-full" src="{{asset('logo/logo_nombre.png')}}" width="120" height="40"
             alt="pro-harm">
        <img class="navbar-brand-minimized" src="{{ asset('logo/logo_figura.png') }}" width="40" height="40"
             alt="pro-harm" style="object-fit: contain">


    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none no-outline" type="button"
            data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    {{--
  <ul class="nav navbar-nav d-md-down-none">
      <li class="nav-item px-3">
          <a class="nav-link" href="#">Dashboard</a>
      </li>
      <li class="nav-item px-3">
          <a class="nav-link" href="#">Users</a>
      </li>
      <li class="nav-item px-3">
          <a class="nav-link" href="#">Settings</a>
      </li>
  </ul>
  --}}


    <ul class="nav navbar-nav ml-auto">
        {{--
         <li class="nav-item d-md-down-none">
             <a class="nav-link" href="#">
                 <i class="icon-list"></i>
             </a>
         </li>
         <li class="nav-item d-md-down-none">
             <a class="nav-link" href="#">
                 <i class="icon-location-pin"></i>
             </a>
         </li>
         --}}
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
               aria-expanded="false">

                     @if(Storage::disk('public')->exists(Auth::user()->staff->image))
                        @php($img = asset('storage').'/'. Auth::user()->staff->image)
                    @else
                        @php($img = asset('admin/img/default-user.svg'))
                    @endif


                <img class="img-avatar" src="{{ $img }}" alt="email de usuario ">

            </a>

        </li>
    </ul>
    <button class="navbar-toggler aside-menu-toggler d-md-down-none  no-outline" type="button"
            data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none  no-outline" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
    </button>
</header>
