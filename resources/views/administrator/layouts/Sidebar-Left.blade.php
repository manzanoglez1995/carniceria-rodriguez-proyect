<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">

            <li class="nav-item">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="nav-icon fa fa-tachometer"></i> Dashboard
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('orders.show') }}">
                    <i class="nav-icon fa fa-cart-arrow-down"></i>Pedidos</a>
            </li>

            @if(Auth::user()->staff->job == "administrador")

                <li class="nav-item">
                    <a class="nav-link" href="{{route('products.show')}}">
                        <i class="nav-icon fa fa-product-hunt"></i>Productos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('categories.show')}}">
                        <i class="nav-icon fa fa-cubes"></i>Categorías</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('customers.show')}}">
                        <i class="nav-icon fa fa-user-o"></i>Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('workers.show')}}">
                        <i class="nav-icon fa fa-user"></i>Empleados</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('config.schedule')}}">
                        <i class="nav-icon fa fa-clock"></i>Horarios</a>
                </li>

            @endif

        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
