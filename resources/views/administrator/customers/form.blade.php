@extends('administrator.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-plus-circle"></i><label>Clientes</label>
        </div>
        <div class="card-body">
            <div class="content">
                <form-customers user_edit="{{isset($user) ? $user : null }}"></form-customers>
            </div>
        </div>
    </div>

@endsection
