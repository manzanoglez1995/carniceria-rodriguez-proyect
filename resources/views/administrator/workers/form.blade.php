@extends('administrator.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-plus-circle"></i><label>Usuarios</label>
        </div>
        <div class="card-body">
            <div class="content">
                <form-workers user_edit="{{isset($user) ? $user : null }}"></form-workers>
            </div>
        </div>
    </div>

@endsection
