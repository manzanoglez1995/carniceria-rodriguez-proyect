@extends('landing_page.layout.layout')

@section('content')

<div class="container padding-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Inicio de sesión</h2>
                </div>
                <br>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Correo electrónico</label>

                            <div class="col-md-7">
                                <input id="email"
                                       type="email"
                                       placeholder="Correo electrónico"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       name="email" value="{{ old('email') }}"
                                       required
                                       autofocus>

                                @if ($errors->has('email'))
                                    <span class="error" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>

                            <div class="col-md-7">
                                <input id="password"
                                       type="password"
                                       placeholder="Contraseña"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password"
                                       required>

                                @if ($errors->has('password'))
                                    <span class="error" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-10 offset-md-10">
                                <button type="submit"
                                        class="btn-block primary-btn order-submit" style="outline: transparent">
                                    {{ __('Iniciar Sesión') }}  </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a>
                                @endif

                                <label for="o">ó</label>

                                <a class="btn btn-link" href="{{ route('register') }}">¿Aún no tienes cuenta? Regístrate</a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <img src="{{asset('logo/logo_original.png')}}" alt="">
        </div>
    </div>
</div>

@endsection
