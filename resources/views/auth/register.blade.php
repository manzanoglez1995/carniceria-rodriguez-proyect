@extends('landing_page.layout.layout')
@section('content')
    <div class="container p-5 padding-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h2 class="card-header" style="margin-bottom: 20px">{{ __('Registro') }}</h2>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}" novalidate>
                            @csrf

                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           placeholder="Nombre"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="error" role="alert">
                                             <strong>{{ $errors->first('name') }}</strong>
                                        </span>

                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Correo electrónico') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           placeholder="Correo electrónico"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="error" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">Teléfono</label>

                                <div class="col-md-6">
                                    <input id="telephone" type="number"
                                           placeholder="celular"
                                           class="form-control{{ $errors->has('telephone') ? ' is-invalid' : '' }}"
                                           name="telephone" value="{{ old('telephone') }}" required>

                                    @if ($errors->has('telephone'))
                                        <span class="error" role="alert">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           placeholder="Contraseña"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="error" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Confirmar contraseña') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm"
                                           placeholder="Confirmar contraseña"
                                           type="password" class="form-control"
                                           name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-10 offset-md-10">
                                    <button type="submit"
                                            class=" btn-block primary-btn order-submit" style="outline: transparent">
                                        {{ __('Registrarme') }}
                                    </button>

                                    <a class="btn btn-link" href="{{ route('login') }}">¿Ya tienes tu cuenta? inicia sesión</a>

                                </div>
                            </div>

                        </form>
                    </div>


                </div>
            </div>

            <div class="col-md-4">
                <img src="{{asset('landing/img/40xp.png')}}" alt="">
            </div>
        </div>
    </div>
@endsection
