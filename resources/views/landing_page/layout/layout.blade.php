<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('logo/favicon.ico') }}">

    <title>Carniceria Rodriguez</title>

    <link rel="stylesheet" href="/landing/css/main.css">
    <link type="text/css" rel="stylesheet" href="/landing/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="/landing/css/slick.css"/>
    <link type="text/css" rel="stylesheet" href="/landing/css/slick-theme.css"/>
    <link type="text/css" rel="stylesheet" href="/landing/css/nouislider.min.css"/>
    <link type="text/css" rel="stylesheet" href="/landing/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/landing/css/style.css"/>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
<div id='app'>
    <!-- HEADER -->
@include("landing_page.layout.header")
<!-- /HEADER -->

@yield('content')

</div>
<!-- FOOTER -->
@include("landing_page.layout.footer")
<!-- /FOOTER -->

</body>

<script src="{{ asset('js/app.js') }}"></script>

<script src="/landing/js/jquery.min.js"></script>
<script src="/landing/js/bootstrap.min.js"></script>
<script src="/landing/js/slick.min.js"></script>
<script src="/landing/js/nouislider.min.js"></script>
<script src="/landing/js/jquery.zoom.min.js"></script>
<script src="/landing/js/main.js"></script>

</html>
