<!-- HEADER -->
<header>
    <!-- TOP HEADER -->
    <div id="top-header">
        <div class="container">
            <ul class="header-links pull-left">
                <li><a href="#"><i class="fa fa-phone"></i>+3322-250-666</a></li>
                <li><a target="_blank" href="https://goo.gl/maps/yAyUyT7WMbze2Nk19"><i class="fa fa-map-marker"></i>María C. Bancalari #3045 col. Echeverria</a></li>
            </ul>
            <ul class="header-links pull-right">
                @guest
                    <li><a href="/register"><i class="fa fa-share"></i>Registrarse</a></li>
                    <li><a href="/login"><i class="fa fa-user-o"></i>Iniciar sesión </a></li>
                @endguest
                @auth
                    <li><i class="fa fa-user-o"></i>
                        <label style="color: white">
                            Usuario: {{Auth::user()->name}}
                        </label>
                    </li>

                    <li><a href=""
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-lock"></i>Cerrar sesión</a>
                        <form id="logout-form" method="POST" action="{{ route('logout') }}">
                            @csrf
                        </form>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
    <!-- /TOP HEADER -->
    <!-- MAIN HEADER -->
    <div class="header-menu">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- LOGO -->
                <div class="col-md-3">
                    <div class="header-logo">
                        <a href="#" class="logo">
                            <img src="{{asset("logo/Logo_blanco_transparente.png")}}" alt="" style="width: 100px;">
                        </a>
                    </div>
                </div>
                <!-- /LOGO -->

                <!-- MENU BAR -->
                <div class="col-md-7 hide_mobile">
                    <!-- NAV -->
                    <ul class="main-nav nav navbar-nav">
                        <li>
                            <a class="beef_white font-medium-bold" href="{{route("landing")}}">Inicio</a>
                        </li>
                        <li>
                            <a class="beef_white font-medium-bold" href="{{route("products")}}">Producto</a>
                        </li>

                        @auth
                            <li>
                                <a class="beef_white font-medium-bold" href="{{route("orders")}}">Mis pedidos</a>
                            </li>
                        @endauth

                        @auth
                            <li>
                                <a class="beef_white font-medium-bold" href="{{route("profile")}}">Mi cuenta</a>
                            </li>
                        @endauth
                        <li>
                            <a class="beef_white font-medium-bold" href="{{route("shopping_cart")}}">Carrito</a>
                        </li>
                        @auth
                            @if (Auth::user()->role == "Staff")
                                <li>
                                    <a class="beef_white font-medium-bold"
                                       href="{{route("dashboard")}}">Administrador</a>
                                </li>
                            @endif

                        @endauth

                    </ul>
                    <!-- /NAV -->

                </div>

                <!-- /MENU BAR -->

                <!-- MENU  COMPLEMENT -->
                <div class="col-md-2 clearfix">

                    <div class="header-ctn">

                        <!-- Cart -->
                        <shopping-cart-drop-down/>
                        <!-- /Cart -->
                    </div>
                    <!-- Menu Toogle -->

                    <navigation-menu-mobil is_auth="{{ Auth::check() }}"></navigation-menu-mobil>
                    <!-- /Menu Toogle -->

                </div>
                <!-- /ACCOUNT -->

            </div>
            <!-- row -->
        </div>
        <!-- container -->

    </div>

    <div id="navigation"></div>

</header>
<!-- /HEADER -->
