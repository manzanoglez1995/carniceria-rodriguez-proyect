<!-- FOOTER -->
<footer id="footer">
    <!-- top footer -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Ubicación</h3>
                        <ul class="footer-links">
                            <li><a target="_blank" href="https://goo.gl/maps/yAyUyT7WMbze2Nk19"><i class="fa fa-map-marker"></i>María C. Bancalari #3045 col. Echeverria</a>
                            </li>
                            <li><a href="#"><i class="fa fa-phone"></i>+3322-250-6664</a></li>
                            <li><a href="mailto:carniceriarodriguez3045@outlook.es"><i class="fa fa-envelope-o"></i>carniceriarodriguez3045@outlook.es</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Categoria</h3>
                        <ul class="footer-links">
                            <li><a href="{{route("landing")}}">Inicio</a></li>
                            <li><a href="{{route("products")}}">Productos</a></li>
                            <li><a href="{{route("shopping_cart")}}">Carrito</a></li>

                        </ul>
                    </div>
                </div>

                <div class="col-md-4 col-xs-6">
                    <div class="footer">
                        @auth
                        <h3 class="footer-title">Cuenta</h3>
                        <ul class="footer-links">

                                <li><a href="{{route("profile")}}">Mis datos</a></li>
                                <li><a href="{{route("orders")}}">Mis pedidos</a></li>
                                @if (Auth::user()->role == "Staff")
                                    <li>
                                        <a href="{{route("dashboard")}}">Administrador</a>
                                    </li>
                                @endif
                        </ul>
                        @endauth
                    </div>
                </div>

            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /top footer -->

    <!-- bottom footer -->
    <div id="bottom-footer" class="section">
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12 text-center">

                    <span class="copyright">
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
								Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Desarrollado por <a
                            href="https://colorlib.com" target="_blank">PRO-HARM</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							</span>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /bottom footer -->
</footer>
<!-- /FOOTER -->
