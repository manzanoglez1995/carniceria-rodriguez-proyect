/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import api from './store/api';
import BootstrapVue from 'bootstrap-vue';
import VueSweetalert2 from 'vue-sweetalert2';
import SortedTablePlugin from "vue-sorted-table";
import Paginate from 'vuejs-paginate'
import VueCurrencyFilter from 'vue-currency-filter'
import VueGoogleCharts from 'vue-google-charts'
import * as VueGoogleMaps from "vue2-google-maps";
import vMultiselectListbox from 'vue-multiselect-listbox'
import 'vue-multiselect-listbox/dist/vue-multi-select-listbox.css';


window.Vue = require('vue');
window.Api = api;
window.Event = new Vue;


Vue.use(BootstrapVue);
Vue.use(VueSweetalert2);
Vue.use(SortedTablePlugin);
Vue.component('paginate', Paginate);
Vue.use(VueGoogleCharts);
Vue.component('v-multiselect-listbox', vMultiselectListbox);

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyCRDW1V64Hxf9QL815Pbr9haAtwP8mnVRQ",
        libraries: "places", // necessary for places inputx
        region: 'MX',
        language: 'es',

    }
});


Vue.use(VueCurrencyFilter, {
    symbol: '$',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
});


const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


const app = new Vue({
    el: '#app',
});
