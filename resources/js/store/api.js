export default {
    users: {
        update: '/api/users/update',
        staff: {
            show: '/api/users/staff/show',
            create: '/api/users/staff/create',
            update: '/api/users/staff/update',
            delete: '/api/users/staff/delete',
        },
        costumer: {
            show: '/api/users/costumer/show',
            create: '/api/users/costumer/create',
            update: '/api/users/costumer/update',
            delete: '/api/users/costumer/delete',
        }

    },
    categories: {
        show: '/api/categories/show',
        topTen: '/api/categories/topTen',
        create: '/api/categories/create',
        update: '/api/categories/update',
        delete: '/api/categories/delete',
    },
    products: {
        show: '/api/products/show',
        create: '/api/products/create',
        update: '/api/products/update',
        delete: '/api/products/delete',
        related_products: '/api/products/related_products',
        getShoppingCart: '/api/products/getShoppingCart',
        topThreeReviews: '/api/products/topThreeReviews',
    },
    reviews: {
        show: '/api/reviews/show',
        create: '/api/reviews/create',
        delete: '/api/reviews/delete',
    },
    schedule: {
        all: '/api/schedule/show',
        update: '/api/schedule/update',
        getAvailableSchedule: '/api/schedule/getAvailableSchedule',
    },
    orders: {
        show: '/api/order/show',
        create: '/api/order/create',
        edit: '/api/order/edit',
        cancel: '/api/order/cancel',
    },
    graphics: '/api/graphics',

}
